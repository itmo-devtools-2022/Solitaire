enum RequestState {
    Pending = 0,
    Success = 1,
    Failure = 2,
}

export default RequestState